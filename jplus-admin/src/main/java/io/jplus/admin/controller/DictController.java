/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Dict;
import io.jplus.admin.service.DictService;
import io.jplus.core.base.BaseController;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Package io.jplus.admin.controller
 */
@RequestMapping(value = "/admin/dict", viewPath = Consts.BASE_VIEW_PATH + "admin/dict")
public class DictController extends BaseController {

    @JbootrpcService
    DictService dictService;

    public void list() {
        Columns columns = Columns.create();
        String dictName = getPara("dictName");
        if (StrKit.notBlank(dictName)) {
            columns.like("name", dictName);
        }
        String orderBy = getPara("orderBy", "id");
        Page<Dict> page = dictService.paginateByColumns(getPageNumber(), getPageSize(), columns.getList(), orderBy);
        renderPageForBT(page);
    }

}
