/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.listener;

import com.jfinal.log.Log;
import io.jboot.JbootConstants;
import io.jboot.event.JbootEvent;
import io.jboot.event.JbootEventListener;
import io.jboot.event.annotation.EventConfig;

@EventConfig(action = {JbootConstants.EVENT_STARTED})
public class JplusEventListener implements JbootEventListener {

    private static final Log log = Log.getLog(JplusEventListener.class);

    @Override
    public void onEvent(JbootEvent event) {
        //Object event.getData();
        log.info("Jplus is started!");
    }
}
