/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;


@JFinalDirective("JplusSelect")
public class JplusSelectDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String name = getParam("name", scope);
        String id = getParam("id", scope);
        String hidden = getParam("hidden", "", scope);
        boolean underline = getParam("underline", false, scope);
        Object hiddenValue = getParam("hiddenValue", "", scope);

        StringBuffer html = new StringBuffer();
        html.append("<div class=\"form-group\">\n" +
                "    <label class=\"col-sm-3 control-label\">" + name + "</label>\n" +
                "    <div class=\"col-sm-9\">\n" +
                "        <select class=\"form-control\" id=\"" + id + "\" name=\"" + id + "\">");
        //先执行
        write(writer, html.toString());
        renderBody(env, scope, writer);
        //后执行
        html.setLength(0);
        html.append("</select>");
        if (StrKit.notBlank(hidden)) {
            html.append(" <input class=\"form-control\" type=\"hidden\" id=\"" + hidden + "\" value=\"" + hiddenValue + "\">");
        }
        html.append(" </div></div>");
        if (underline) {
            html.append("<div class=\"hr-line-dashed\"></div>");
        }
        write(writer, html.toString());
    }


    @Override
    public boolean hasEnd() {
        return true;
    }
}
