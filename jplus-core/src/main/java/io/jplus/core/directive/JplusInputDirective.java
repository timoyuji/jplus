/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.exception.JbootException;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

@JFinalDirective("JplusInput")
public class JplusInputDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String id = getParam("id", scope);
        String name = getParam("name", scope);
        Object value = getParam("value", scope);
        String type = getParam("type", "text", scope);
        String readonly = getParam("readonly", "", scope);
        String onclick = getParam("onclick", "", scope);
        String style = getParam("style", "", scope);
        String disabled = getParam("disabled", "", scope);
        boolean underline = getParam("underline", false, scope);
        String hidden = getParam("hidden", "", scope);
        Object hiddenValue = getParam("hiddenValue", "", scope);
        if (StrKit.isBlank(id)) {
            throw new JbootException("JplusInput id is not null!");
        }
        if (StrKit.isBlank(name)) {
            throw new JbootException("JplusInput name is not null!");
        }
        StringBuffer html = new StringBuffer();
        html.append("<div class=\"form-group\"> <label class=\"col-sm-3 control-label\">").append(name).append("</label> <div class=\"col-sm-9\">");
        html.append(" <input class=\"form-control\" id=\"").append(id).append("\" name=\"").append(id).append("\"");
        if (value != null) {
            html.append(" value=\"").append(value).append("\"");
        }
        html.append("type=\"").append(type).append("\"");
        if (StrKit.notBlank(readonly)) {
            html.append("readonly = \"readonly\"");
        }
        if (StrKit.notBlank(onclick)) {
            html.append("onclick=\"").append(onclick).append("\"");
        }
        if (StrKit.notBlank(style)) {
            html.append("style=\"").append(style).append("\"");
        }
        if (StrKit.notBlank(disabled)) {
            html.append("disabled=\"").append(disabled).append("\"");
        }
        html.append(">");
        if (StrKit.notBlank(hidden)) {
            html.append("<input class=\"form-control\" type=\"hidden\" id=\"").append(hidden).append("\" value=\"").append(hiddenValue).append("\">");
        }
        html.append("</div></div>");
        if (underline) {
            html.append("<div class=\"hr-line-dashed\"></div>");
        }
        write(writer, html.toString());
    }

}
