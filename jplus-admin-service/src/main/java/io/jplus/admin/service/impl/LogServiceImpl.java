package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Column;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Log;
import io.jplus.admin.service.LogService;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Bean
@Singleton
@JbootrpcService
public class LogServiceImpl extends JbootServiceBase<Log> implements LogService {

    @Override
    public Page<Log> paginateByColumns(int page, int size, List<Column> columns, String orderBy) {
        return DAO.paginateByColumns(page, size, columns, orderBy);
    }

    @Override
    public boolean deleteAll() {
        List<Log> logList = DAO.findAll();
        List<Integer> delList = new ArrayList<>(logList.size());
        for (Log log : logList) {
            delList.add(log.getId());
        }
        return DAO.deleteById(logList);
    }
}